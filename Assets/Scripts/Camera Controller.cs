using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform target;
    private Vector3 offset;

    void Start()
    {
        // Attempt to find the "Player" GameObject by tag
        GameObject player = GameObject.FindGameObjectWithTag("Player");

        if (player != null)
        {
            target = player.transform;
            offset = transform.position - target.position;
        }
        else
        {
            Debug.LogError("Player GameObject not found. Make sure you have a GameObject with the 'Player' tag in the scene.");
        }
    }

    void LateUpdate()
    {
        // Check if target is not null before using it
        if (target != null)
        {
            Vector3 newPosition = new Vector3(transform.position.x, transform.position.y, offset.z + target.position.z);
            transform.position = Vector3.Lerp(transform.position, newPosition, 10 * Time.deltaTime);
        }
    }
}
