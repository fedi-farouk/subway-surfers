using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    private CharacterController controller;
 
    private Vector3 direction;
    public float forwardSpeed;
    private int desiredLane = 1;//0:left, 1:middle, 2:right
    public float laneDistance = 3;//The distance between tow lanes
    public float jumpForce;
    public float Gravity = -20;
    public bool isGrounded;
    public LayerMask groundLayer;
    public Transform groundCheck;
    private bool Sliding = false;
    public float slideDuration = 1;
    private Animator animator;
    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

    }


    void Update()
    {
        if (!PlayerManager.isGameStarted)
           
        return;
        controller.Move(direction * Time.deltaTime);
        direction.z = forwardSpeed;




        if (controller.isGrounded)
        {
            direction.y = -1;
            if (SwipeManager.swipeUp)
            {
                Jump();
            }
        }
        else
        {
            direction.y += Gravity * Time.deltaTime;
        }

       
        if (SwipeManager.swipeRight)
        {
            desiredLane++;
            if (desiredLane == 3)
                desiredLane = 2;
        }
        if (SwipeManager.swipeLeft)
        {
            desiredLane--;
            if (desiredLane == -1)
                desiredLane = 0;
        }

        Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;
        if (desiredLane == 0)
            targetPosition += Vector3.left * laneDistance;
        else if (desiredLane == 2)
            targetPosition += Vector3.right * laneDistance;
        transform.position = Vector3.Lerp(transform.position, targetPosition, 50 * Time.deltaTime);

    }
    
    private Boolean Jump()
    {
        AudioManager.instance.PlaySFX("Jump");
        direction.y = jumpForce;
        return true;
    }

  

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

        if (hit.transform.tag == "Obstacle")
        {

            AudioManager.instance.PlaySFX("GameOver");
            PlayerManager.gameOver = true;

        }
    }
    private IEnumerator Slide()
    {
        Sliding = true;
       
        animator.SetBool("Sliding", true);
      
        yield return new WaitForSeconds(0.25f / Time.timeScale);

        yield return new WaitForSeconds((slideDuration - 0.25f) / Time.timeScale);
        animator.SetBool("Sliding", false);

        Sliding = false;
    }
}