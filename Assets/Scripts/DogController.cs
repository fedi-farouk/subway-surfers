using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;

public class DogController : MonoBehaviour
{
    public Transform target; // The target character to follow
    public float moveSpeed; // The speed at which the follower character moves

    private Animator animator; // The animator component of the follower character

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
  
        float distanceToTarget = Vector3.Distance(transform.position, target.position);
        if (!PlayerManager.isGameStarted)
            return;
        if (distanceToTarget > 1.0f)
        {
         
            Vector3 moveDirection = (target.position - transform.position).normalized;

            animator.SetBool("Speed", true);

            transform.Translate(moveDirection * moveSpeed * Time.deltaTime, Space.World);
        }
        else
        {

            animator.SetFloat("Speed", 0.0f);
        }
    }
}
