using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Text coinText; 

    void Start()
    {
        
        int savedCoins = PlayerPrefs.GetInt("playerScore", 0);
        coinText.text = "" + savedCoins;
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Level");
    }

    public void Charactere()
    {
        SceneManager.LoadScene("Charactere");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
