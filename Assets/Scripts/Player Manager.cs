using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static bool gameOver;
    public GameObject gameOverPanel;
    public static bool isGameStarted;
    public GameObject startingText;
    public static int numberOfCoins;
    public Text coinsText;

    private string playerScore = "playerScore";

    void Start()
    {
        // Load the saved score from PlayerPrefs and display it in the UI Text.
        int savedCoins = PlayerPrefs.GetInt(playerScore, 0);
        numberOfCoins = 0; // Reset the in-game score to 0.

        gameOver = false;
        Time.timeScale = 1;
        isGameStarted = false;

        coinsText.text = "" + numberOfCoins;
    }

    void Update()
    {
        if (gameOver)
        {
            
            int previousScore = PlayerPrefs.GetInt(playerScore, 0);
            if (numberOfCoins > previousScore)
            {
                PlayerPrefs.SetInt(playerScore, numberOfCoins);
                PlayerPrefs.Save();
            }

            Time.timeScale = 0;
            gameOverPanel.SetActive(true);
        }

        coinsText.text = "" + numberOfCoins;

        if (SwipeManager.tap && !isGameStarted)
        {
            isGameStarted = true;
            Destroy(startingText);
        }
    }
}
