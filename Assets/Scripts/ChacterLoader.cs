using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterLoader : MonoBehaviour
{
    public GameObject[] characterPrefabs;
    private int selectedCharacterIndex;

    private void Start()
    {
        selectedCharacterIndex = PlayerPrefs.GetInt("SelectedCharacterIndex", 0);
        Vector3 spawnPosition = transform.position - Vector3.up;
        GameObject selectedCharacter = Instantiate(characterPrefabs[selectedCharacterIndex], spawnPosition, transform.rotation);
        selectedCharacter.transform.localScale = transform.localScale;
        selectedCharacter.transform.parent = transform;
    }
}


