using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour
{
    public GameObject[] characterPrefabs;
    private int selectedCharacterIndex = 0;
    private GameObject selectedCharacter;

    public Button nextButton;
    public Button previousButton;
    public Button selectButton;
    public Button exitButton;

    private Transform capsule;
    public Text coin;

    private void Start()
    {
        int savedCoins = PlayerPrefs.GetInt("playerScore", 0);
        coin.text = "" + savedCoins;
        nextButton.onClick.AddListener(NextCharacter);
        previousButton.onClick.AddListener(PreviousCharacter);
        selectButton.onClick.AddListener(SelectCharacter);
        exitButton.onClick.AddListener(ExitGame);

        capsule = transform.Find("Capsule"); 

        selectedCharacter = Instantiate(characterPrefabs[selectedCharacterIndex], capsule.position, capsule.rotation);
        selectedCharacter.transform.localScale = capsule.localScale;
    }

    public void NextCharacter()
    {
       
        Destroy(selectedCharacter);

       
        selectedCharacterIndex = (selectedCharacterIndex + 1) % characterPrefabs.Length;

       
        selectedCharacter = Instantiate(characterPrefabs[selectedCharacterIndex], capsule.position, capsule.rotation);
        selectedCharacter.transform.localScale = capsule.localScale;
    }

    public void PreviousCharacter()
    {
        
        Destroy(selectedCharacter);

       
        selectedCharacterIndex = (selectedCharacterIndex - 1 + characterPrefabs.Length) % characterPrefabs.Length;

        
        selectedCharacter = Instantiate(characterPrefabs[selectedCharacterIndex], capsule.position, capsule.rotation);
        selectedCharacter.transform.localScale = capsule.localScale;
    }

    public void SelectCharacter()
    {
        PlayerPrefs.SetInt("SelectedCharacterIndex", selectedCharacterIndex);
        PlayerPrefs.Save(); 
        Debug.Log("Selected character: " + selectedCharacterIndex);
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Menu");
        Application.Quit(); // Ensure that the "Quit" option is enabled in Build Settings
    }
}
