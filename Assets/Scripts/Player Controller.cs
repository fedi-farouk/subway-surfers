using System;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController controller;
    private Animator animator;
    private Vector3 direction;
    public float forwardSpeed;
    private int desiredLane = 1;
    public float laneDistance = 3;
    public float jumpForce;
    public float Gravity = -20;
    public bool isGrounded;
    public LayerMask groundLayer;
    public Transform groundCheck;
    private bool Sliding = false;
    public float slideDuration = 1;
 

    void Start()
    {
       
        animator = GetComponent<Animator>();

    }


    void Update()
    {
        if (!PlayerManager.isGameStarted)
           
        return;

        animator.SetBool("run", true);


        if (SwipeManager.swipeDown )
        {
            StartCoroutine(Slide());
            
        }

    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

        if (hit.transform.tag == "Obstacle")
        {
           
            animator.SetTrigger("dead");
            PlayerManager.gameOver = true;

        }
    }
    private IEnumerator Slide()
    {
        Sliding = true;
        animator.SetBool("Sliding", true);
        yield return new WaitForSeconds(0.25f / Time.timeScale);
   
        yield return new WaitForSeconds((slideDuration - 0.25f) / Time.timeScale);
        animator.SetBool("Sliding", false);
       
        Sliding = false;
    }

}